﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CwiczeniaWWW.Migrations
{
    public partial class CarModuleChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsInStock",
                table: "Cars",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsInStock",
                table: "Cars");
        }
    }
}
