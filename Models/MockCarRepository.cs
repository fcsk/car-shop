﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CwiczeniaWWW.Models
{
    public class MockCarRepository : ICarRepository
    {
        private List<Car> _Cars;
        
        public MockCarRepository()
        {
            if(_Cars==null)
            {
                InitializeCars();
            }
        }

        private void InitializeCars()
        {
            _Cars = new List<Car>
            {
                new Car { Id = 1, Name= "Maluch",Price = 5200.95M,ShortDescription="Maluszek", LongDescription="Longblabla", ImageThumbnailUrl="http://www.gieldaklasykow.pl/wp-content/uploads/2013/07/1.jpg" },
                new Car { Id = 2, Name = "Lanos", Price = 3000.95M, ShortDescription = "Koreańskie Ferrari", LongDescription = "Long blabla",ImageThumbnailUrl="https://www.tapeciarnia.pl/tapety/normalne/40403_srebrny_daewoo_lanos_sedan.jpg" },
                new Car { Id = 3, Name = "125p", Price = 8000.95M, ShortDescription = "Zadymiacz", LongDescription = "Long blabla", ImageThumbnailUrl="http://s3.manifo.com/usr/f/ff540/28/manager/uoxpehnl6gpfanglnb.jpg" }
             };  
        }


        public IEnumerable<Car> GetAllCar()
        {
            return _Cars;
        }

        public Car GetCarById(int carId)
        {
            return _Cars.FirstOrDefault(p => p.Id == carId);
        }
    }
}
