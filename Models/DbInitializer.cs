﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CwiczeniaWWW.Models
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            if (!context.Cars.Any())
            {
                context.AddRange
                    (
                        new Car { Name = "Maluch", Price = 4002.95M, ShortDescription = "Maluszek", LongDescription = "sadas", ImageUrl = "http://www.gieldaklasykow.pl/wp-content/uploads/2013/07/1.jpg", IsCarOfTheWeek = true, ImageThumbnailUrl = "http://www.gieldaklasykow.pl/wp-content/uploads/2013/07/1.jpg" },
                        new Car { Name = "Corsa", Price = 40000.95M, ShortDescription = "Corsa love", LongDescription = "asdasdasdasdadsads.", ImageUrl = "https://thewallpapers.org/zoom/45368/Opel-Corsa-OPC-Nurburgring-Edition-007.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "https://thewallpapers.org/zoom/45368/Opel-Corsa-OPC-Nurburgring-Edition-007.jpg" },
                        new Car { Name = "Lanos", Price = 3000.95M, ShortDescription = " Plain pleasure.", LongDescription = "dasdasdasdasdq32e21e21e12e.", ImageUrl = "https://www.tapeciarnia.pl/tapety/normalne/40403_srebrny_daewoo_lanos_sedan.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "https://www.tapeciarnia.pl/tapety/normalne/40403_srebrny_daewoo_lanos_sedan.jpg" },
                        new Car { Name = "Laguna", Price = 1500.95M, ShortDescription = "A summer classic!", LongDescription = "ein zwei drei.", ImageUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/cherrypie.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "http://www.hdcarwallpapers.com/walls/renault_laguna_coupe_2012-wide.jpg" },
                        new Car { Name = "FocuS", Price = 50000.95M, ShortDescription = "Happy holidays with this car!", LongDescription = "Zwerasd asd a", ImageUrl = "http://www.hdcarwallpapers.com/walls/renault_laguna_coupe_2012-wide.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJ-Bgg2ydyZ4CZr1x9U8sYB4mYG4Iue8kdVKFL9uAtV0HQ1ah8" },
                        new Car { Name = "A3", Price = 70000.95M, ShortDescription = "A Christmas favorite", LongDescription = " gummies.", ImageUrl = "http://www.hdcarwallpapers.com/walls/2015_audi_a3_cabriolet_sport-HD.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "http://www.hdcarwallpapers.com/walls/2015_audi_a3_cabriolet_sport-HD.jpg" },//
                        new Car { Name = "Passat", Price = 70000.95M, ShortDescription = "Sweet as Ferrarir", LongDescription = ". raz dwa trzy.", ImageUrl = "https://wallpapercave.com/wp/wp1943311.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "https://wallpapercave.com/wp/wp1943311.jpg" },//
                        new Car { Name = "Trabant", Price = 20000.95M, ShortDescription = "Our Halloween favorite", LongDescription = "umzikulu.", ImageUrl = "https://www.tapeciarnia.pl/tapety/normalne/tapeta-trabant-601.jpg", IsCarOfTheWeek = true, ImageThumbnailUrl = "https://www.tapeciarnia.pl/tapety/normalne/tapeta-trabant-601.jpg" },//
                        new Car { Name = "Golf", Price = 70000.95M, ShortDescription = "My God, so good car!", LongDescription = "Uno duo.", ImageUrl = "https://static.carthrottle.com/workspace/uploads/posts/2015/12/99380a4646832a2f012501ebfb65124c.jpg", IsCarOfTheWeek = true, ImageThumbnailUrl = "https://static.carthrottle.com/workspace/uploads/posts/2015/12/99380a4646832a2f012501ebfb65124c.jpg" },//
                        new Car { Name = "Leon", Price = 55000.95M, ShortDescription = "Our special!", LongDescription = "    .", ImageUrl = "https://www.carpixel.net/w/87fadc9a6394281f0d04869241963ac6/seat-leon-sc-cupra-300-car-wallpaper-64177.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "https://www.carpixel.net/w/87fadc9a6394281f0d04869241963ac6/seat-leon-sc-cupra-300-car-wallpaper-64177.jpg" },
                        new Car { Name = "Rumak", Price = 12000.95M, ShortDescription = "You'll love it!", LongDescription = " ", ImageUrl = "https://s1.cdn.autoevolution.com/images/news/gallery/tazzari-zero-priced-at-18000-euros_3.jpg", IsCarOfTheWeek = false, ImageThumbnailUrl = "https://s1.cdn.autoevolution.com/images/news/gallery/tazzari-zero-priced-at-18000-euros_3.jpg" }
            );
                context.SaveChanges(); 
            }
        }
    }
}
