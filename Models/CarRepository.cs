﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CwiczeniaWWW.Models
{
    public class CarRepository:ICarRepository
    {
        private readonly AppDbContext _appDbContext;

        public CarRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Car> GetAllCar()
        {
            return _appDbContext.Cars;
        }

        public Car GetCarById(int carId)
        {
            return _appDbContext.Cars.FirstOrDefault(p => p.Id == carId);
        }
    }
}
