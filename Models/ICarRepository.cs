﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CwiczeniaWWW.Models
{
    public interface ICarRepository
    {
        IEnumerable<Car> GetAllCar();
        Car GetCarById(int carId);
    }
}
