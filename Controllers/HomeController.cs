﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CwiczeniaWWW.Models;
using CwiczeniaWWW.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CwiczeniaWWW.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICarRepository _carRepository;

        public HomeController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {

            var Cars = _carRepository.GetAllCar().OrderBy(p => p.Name);

            var homeViewModel = new HomeViewModel()
            {
                Title = "Witam w moim sklepie samochodowym",
                Cars = Cars.ToList()
        };
            return View(homeViewModel);
        }

        public IActionResult Details(int id)
        {
            var car = _carRepository.GetCarById(id);
            if (car == null)
                return NotFound();
            return View(car);
        }
    }
}
